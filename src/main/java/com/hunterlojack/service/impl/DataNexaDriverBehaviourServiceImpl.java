package com.hunterlojack.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.hunterlojack.client.Tracking3dClient;
import com.hunterlojack.entity.RepNexaDriverBehaviourEntity;
import com.hunterlojack.entity.RepUsuarioEntity;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.report.data.DataResultResponseDTO;
import com.hunterlojack.model.report.data.LogDataResultResponseDTO;
import com.hunterlojack.model.report.list.LogListResultResponseDTO;
import com.hunterlojack.repository.RepNexaDriverBehaviourRepository;
import com.hunterlojack.repository.RepUsuarioRepository;
import com.hunterlojack.service.DataNexaDriverBehaviourService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DataNexaDriverBehaviourServiceImpl implements DataNexaDriverBehaviourService {

    private final Tracking3dClient tracking3dClient;
    private final RepNexaDriverBehaviourRepository repNexaDriverBehaviourRepository;
    private final RepUsuarioRepository repUsuarioRepository;

    public DataNexaDriverBehaviourServiceImpl(
        Tracking3dClient tracking3dClient,
        RepNexaDriverBehaviourRepository repNexaDriverBehaviourRepository,
        RepUsuarioRepository repUsuarioRepository){
        this.tracking3dClient = tracking3dClient;
        this.repNexaDriverBehaviourRepository = repNexaDriverBehaviourRepository;
        this.repUsuarioRepository = repUsuarioRepository;
    }

    @Override
    public void getAllDataOfService(){
        //obtener los usuarios de POWER BI
        RepUsuarioEntity user = repUsuarioRepository.findByUsuario("powerbinexa");

        //insertar nueva informacion
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setPass(user.getContrasena());
        loginRequestDTO.setUser(user.getUsuario());

        //obtener informacion de reportLogList
        List<LogListResultResponseDTO> resultsTmp = tracking3dClient.reportRunLogList(loginRequestDTO);
        log.info("obtener informacion de reportLogList: {}", resultsTmp);

        LogListResultResponseDTO logListResultResponseDTO =
            resultsTmp.stream().filter(x -> {
                log.info("item para filtrar vehicle details: {}", x);
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime runDate = x.getRunDate();
                return "driver Behaviour".equalsIgnoreCase(x.getReportType())
                    && x.getScheduleName().toLowerCase().startsWith("powerbi")
                    && now.getMonthValue() == runDate.getMonthValue()
                    && now.getYear() == runDate.getYear()
                    && now.getDayOfMonth() == runDate.getDayOfMonth();
            }).findAny().orElse(null);

        if (logListResultResponseDTO == null) {
            log.info("no hay imformacion, logListResultReponseDTO is null");
            return;
        }

        LogDataResultResponseDTO logDataResultResponseDTO;
        do {
            logDataResultResponseDTO =
                tracking3dClient.reportRunLogData(loginRequestDTO, logListResultResponseDTO.getReportRunLogUid())
                    .orElse(null);
        } while (logDataResultResponseDTO == null);

        List<DataResultResponseDTO> dataResultResponsDTOS =
            logDataResultResponseDTO.getDataResultResponsDTOS() == null
                ? new ArrayList<>() : logDataResultResponseDTO.getDataResultResponsDTOS();

        for (DataResultResponseDTO dataResultResponseDTO : dataResultResponsDTOS) {
            List<String> columns = dataResultResponseDTO.getColumns();
            for (Map<String, String> row : dataResultResponseDTO.getRows()) {
                log.info("mapa para obtener informacion {}", row);
                RepNexaDriverBehaviourEntity entity = new RepNexaDriverBehaviourEntity();
                for (int i = 0; i < columns.size(); i++) {
                    String column = columns.get(i);
                    String value = row.get(column);
                    switch (i) {
                        case 0:
                            entity.setVehiculo(value);
                            break;
                        case 1:
                            entity.setDistancia(value);
                            break;
                        case 2:
                            entity.setFrenadoBrusco(value);
                            break;
                        case 3:
                            entity.setAceleracionAgresiva(value);
                            break;
                        case 4:
                            entity.setVueltaRapidaCambioCarril(value);
                            break;
                        case 5:
                            entity.setAccidente(value);
                            break;
                        case 6:
                            entity.setTiempoRalenti(value);
                            break;
                        case 7:
                            entity.setTotalSpeedingTime(value);
                            break;
                        case 8:
                            entity.setPuntuacion(value);
                            break;
                        default:
                            break;
                    }
                }
                entity.setReportRunLogUid(logDataResultResponseDTO.getReportRunLogUid());
                entity.setRunDate(logDataResultResponseDTO.getRunDate());
                entity.setFecha(
                    logDataResultResponseDTO.getRunDate() != null ? logDataResultResponseDTO.getRunDate().minusDays(1)
                        : LocalDateTime.now().minusDays(1));
                entity.setRepUsuarioEntity(user);
                repNexaDriverBehaviourRepository.save(entity);
            }
        }
    }

}
