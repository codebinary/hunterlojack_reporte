package com.hunterlojack.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import org.springframework.data.annotation.CreatedDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RPB_MAR_REPORTE_COMBINADO")
public class RepMarCombinadoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String placa;
    private String conductor;
    private LocalDateTime fechaInicio;
    private LocalDateTime fechaFinal;
    private String localizacionInicial;
    private String velocidadMaxima;
    private String localizacion;
    private String viaje;
    private LocalTime duracion;
    private Double distanciaKm;

    //campos nuevos
    private String limiteVelocidad;
    private String rangoExceso;
    private String grupoCliente;
    private Double velocidadPromedioMs;
    private String evento;
    private String tag;

    private LocalDateTime runDate;

    @Column(name = "created_at")
    @CreatedDate
    private Date createdAt;

}
