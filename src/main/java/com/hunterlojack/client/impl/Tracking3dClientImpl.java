package com.hunterlojack.client.impl;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.hunterlojack.client.Tracking3dClient;
import com.hunterlojack.model.auth.AuthResultModel;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.auth.UserAuthResponseDTO;
import com.hunterlojack.model.report.data.LogDataResultResponseDTO;
import com.hunterlojack.model.report.data.ReportRunLogDataResponseDTO;
import com.hunterlojack.model.report.data.position.DataPositionListResponseDTO;
import com.hunterlojack.model.report.data.position.PositionResultResponseDTO;
import com.hunterlojack.model.report.list.LogListResultResponseDTO;
import com.hunterlojack.model.report.list.ReportRunLogListModel;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class Tracking3dClientImpl implements Tracking3dClient {

    public static final String URL_PATH = "https://api.3dtracking.net/api/v1.0";
    public static final String URL_REPORT_DATA_LOG = URL_PATH + "/Reports/ReportRunLogData";
    public static final String URL_AUTHENTICATION = URL_PATH + "/Authentication/UserAuthenticate";
    public static final String URL_REPORT_LOG_LIST = URL_PATH + "/Reports/ReportRunLogList";
    public static final String URL_REPORT_DATA_POSITION_LIST = URL_PATH + "/Data/PositionsList";

    public static final String USER_ID_GUID = "userIdGuid";
    public static final String SESSIONID = "sessionid";
    public static final String UID = "Uid";
    public static final String START_ID = "startId";
    public static final String USER_NAME = "UserName";
    public static final String PASSWORD = "Password";

    private final RestTemplate restTemplate;

    public Tracking3dClientImpl(RestTemplate restTemplate){
        this.restTemplate = restTemplate;
    }

    @Override
    public UserAuthResponseDTO authentication(
        String user,
        String pass){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        // Query parameters
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_AUTHENTICATION)
            .queryParam(USER_NAME, user)
            .queryParam(PASSWORD, pass);
        return restTemplate.getForObject(builder.toUriString(), UserAuthResponseDTO.class);
    }

    private Optional<AuthResultModel> authentication(
        LoginRequestDTO loginRequestDTO){
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Collections.singletonList(MediaType.APPLICATION_JSON));
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_AUTHENTICATION)
            .queryParam(USER_NAME, loginRequestDTO.getUser())
            .queryParam(PASSWORD, loginRequestDTO.getPass());
        UserAuthResponseDTO authResponseDTO =
            restTemplate.getForObject(builder.toUriString(), UserAuthResponseDTO.class);
        log.info("obtener login para enviar las credenciales {}", authResponseDTO);
        return (authResponseDTO == null || authResponseDTO.getAuthResultModel() == null) ? Optional.empty()
            : Optional.of(authResponseDTO.getAuthResultModel());
    }

    @Override
    public ReportRunLogListModel reportRunLogList(AuthResultModel auth){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_LOG_LIST)
            .queryParam(USER_ID_GUID, auth.getUserIdGuid())
            .queryParam(SESSIONID, auth.getSessionId());
        String url2 = builder.toUriString();
        return restTemplate.getForObject(url2, ReportRunLogListModel.class);
    }

    @Override
    public List<LogListResultResponseDTO> reportRunLogList(LoginRequestDTO loginRequestDTO){
        AuthResultModel auth = this.authentication(loginRequestDTO).orElse(null);
        if (auth != null) {
            log.info("obtener login para enviar las credenciales {}", auth);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_LOG_LIST)
                .queryParam(USER_ID_GUID, auth.getUserIdGuid())
                .queryParam(SESSIONID, auth.getSessionId());
            String url2 = builder.toUriString();

            ReportRunLogListModel reportRunLogListModel = restTemplate.getForObject(url2, ReportRunLogListModel.class);
            log.info("resultado de la peticion al servicio: {}", reportRunLogListModel);

            return reportRunLogListModel == null || CollectionUtils.isEmpty(reportRunLogListModel.getResults())
                ? Collections.emptyList() : reportRunLogListModel.getResults();
        }
        return Collections.emptyList();
    }

    @Override
    public ReportRunLogDataResponseDTO reportRunLogData(
        AuthResultModel auth,
        String uid){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_DATA_LOG)
            .queryParam(USER_ID_GUID, auth.getUserIdGuid())
            .queryParam(SESSIONID, auth.getSessionId())
            .queryParam(UID, uid);
        String url2 = builder.toUriString();
        return restTemplate.getForObject(url2, ReportRunLogDataResponseDTO.class);
    }

    @Override
    public Optional<LogDataResultResponseDTO> reportRunLogData(
        LoginRequestDTO loginRequestDTO,
        String uid){
        AuthResultModel auth = this.authentication(loginRequestDTO).orElse(null);
        if (auth != null) {
            log.info("obtener login para enviar las credenciales {}", auth);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_DATA_LOG)
                .queryParam(USER_ID_GUID, auth.getUserIdGuid())
                .queryParam(SESSIONID, auth.getSessionId())
                .queryParam(UID, uid);
            String url2 = builder.toUriString();

            ReportRunLogDataResponseDTO
                reportRunLogDataResponseDTO = restTemplate.getForObject(url2, ReportRunLogDataResponseDTO.class);

            log.info("respuesta de obtener info del reporte por id: {}", reportRunLogDataResponseDTO);
            return reportRunLogDataResponseDTO == null || reportRunLogDataResponseDTO.getResult() == null
                ? Optional.empty() : Optional.of(reportRunLogDataResponseDTO.getResult());
        }
        return Optional.empty();
    }

    @Override
    public DataPositionListResponseDTO dataPositionList(
        AuthResultModel auth,
        String startId){
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_DATA_POSITION_LIST)
            .queryParam(USER_ID_GUID, auth.getUserIdGuid())
            .queryParam(START_ID, startId)
            .queryParam(SESSIONID, auth.getSessionId());
        String url2 = builder.toUriString();
        return restTemplate.getForObject(url2, DataPositionListResponseDTO.class);
    }

    @Override
    public Optional<PositionResultResponseDTO> dataPositionList(
        LoginRequestDTO loginRequestDTO,
        String startId){
        AuthResultModel auth = this.authentication(loginRequestDTO).orElse(null);
        if (auth != null) {
            log.info("obtener login para enviar las credenciales {}", auth);
            UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_DATA_POSITION_LIST)
                .queryParam(USER_ID_GUID, auth.getUserIdGuid())
                .queryParam(START_ID, startId)
                .queryParam(SESSIONID, auth.getSessionId());
            String url2 = builder.toUriString();

            DataPositionListResponseDTO
                dataPositionListResponseDTO = restTemplate.getForObject(url2, DataPositionListResponseDTO.class);
            log.info("resultado dataPositionListModel: {}", dataPositionListResponseDTO);
            return dataPositionListResponseDTO == null || dataPositionListResponseDTO.getResult() == null
                ? Optional.empty() : Optional.of(dataPositionListResponseDTO.getResult());
        }
        return Optional.empty();
    }

    @Override
    public DataPositionListResponseDTO dataPositionList(AuthResultModel auth){
        // Query parameters
        UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(URL_REPORT_DATA_POSITION_LIST)
            .queryParam(USER_ID_GUID, auth.getUserIdGuid())
            .queryParam(SESSIONID, auth.getSessionId());

        String url2 = builder.toUriString();
        return restTemplate.getForObject(url2, DataPositionListResponseDTO.class);
    }

}
