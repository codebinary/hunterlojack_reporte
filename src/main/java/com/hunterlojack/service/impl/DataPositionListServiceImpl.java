package com.hunterlojack.service.impl;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.hunterlojack.client.Tracking3dClient;
import com.hunterlojack.dto.RepMarcobreDataPositionDTO;
import com.hunterlojack.entity.RepMarCombinadoEntity;
import com.hunterlojack.entity.RepMarDataPositionListEntity;
import com.hunterlojack.entity.RepUsuarioEntity;
import com.hunterlojack.entity.RepVehiculoEntity;
import com.hunterlojack.mapper.RepDataPositionListMapper;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.excel.ExcelPositionModel;
import com.hunterlojack.model.report.data.position.DataPositionResponseDTO;
import com.hunterlojack.model.report.data.position.PositionResultResponseDTO;
import com.hunterlojack.repository.RepCombinadoRepository;
import com.hunterlojack.repository.RepDataPositionListRepository;
import com.hunterlojack.repository.RepUsuarioRepository;
import com.hunterlojack.repository.RepVehiculoRepository;
import com.hunterlojack.service.DataPositionListService;

import lombok.extern.slf4j.Slf4j;

import static java.util.stream.Collectors.groupingBy;

@Slf4j
@Service
public class DataPositionListServiceImpl implements DataPositionListService {

    private final Tracking3dClient tracking3dClient;
    private final RepDataPositionListRepository repDataPositionListRepository;
    private final RepVehiculoRepository repVehiculoRepository;
    private final RepUsuarioRepository repUsuarioRepository;
    private final RepCombinadoRepository repCombinadoRepository;

    public DataPositionListServiceImpl(
        Tracking3dClient tracking3dClient,
        RepDataPositionListRepository repDataPositionListRepository,
        RepVehiculoRepository repVehiculoRepository,
        RepUsuarioRepository repUsuarioRepository,
        RepCombinadoRepository repCombinadoRepository){
        this.tracking3dClient = tracking3dClient;
        this.repDataPositionListRepository = repDataPositionListRepository;
        this.repVehiculoRepository = repVehiculoRepository;
        this.repUsuarioRepository = repUsuarioRepository;
        this.repCombinadoRepository = repCombinadoRepository;
    }

    @Override
    public void processDataPositionList(){
        //Iniciamos sesion
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setUser("powerbimarcobre");
        loginRequestDTO.setPass("powerbi22!");

        LocalDateTime currentDate = LocalDateTime.now();
        String startId = "";
        String startIdTmp = "0";
        do {
            log.info("inicio de proceso de obtener data del servicio");
            //obtener informacion de reportLogList
            PositionResultResponseDTO result;
            do {
                result = tracking3dClient.dataPositionList(loginRequestDTO, startId).orElse(null);
                if (result == null) {
                    log.info("el resultado es null, ejecutar nuevamente");
                }
            } while (result == null);

            startId = String.valueOf(result.getStartId());
            log.info("valor del startId del servicio: {}", startId);

            //ordenar por tiempo local
            List<DataPositionResponseDTO> dataPositionResponseDTOS = result.getDataPositionResponseDTOS()
                .stream()
                .sorted(Comparator.comparing(DataPositionResponseDTO::getGpsTimeLocal))
                .collect(Collectors.toList());

            List<RepMarDataPositionListEntity> entities = new ArrayList<>();
            //guardar la informacion en tabla
            for (DataPositionResponseDTO dataPositionResponseDTO : dataPositionResponseDTOS) {
                log.info("entidad del servicio: {}", dataPositionResponseDTO);
                RepMarDataPositionListEntity entity =
                    RepDataPositionListMapper.mapper.toEntity(dataPositionResponseDTO);
                entity.setStartId(startIdTmp);

                //Evaluamos que el registro que ingresa tiene la fecha pasada al actual (dia anterior)
                if (isCurrentDate(currentDate, entity)) {
                    startId = "";
                    log.info("entidad ultima en guardar: {}", entity);
                    log.info("el startId es vacio, salimos del while");
                    break;
                }
                log.info("entidad del servicio: {}", dataPositionResponseDTO);
                entities.add(entity);
            }
            log.info("inicio de guardar los {} registro", dataPositionResponseDTOS.size());
            repDataPositionListRepository.saveAll(entities);
            log.info("fin de guardar los {} registro", dataPositionResponseDTOS.size());
            startIdTmp = startId;
        } while (!startId.equals(""));
    }

    private boolean isCurrentDate(
        LocalDateTime currentDate,
        RepMarDataPositionListEntity entity){
        int monthCurrent = currentDate.getMonthValue();
        int dayOfMonthCurrent = currentDate.getDayOfMonth();
        int yearCurrent = currentDate.getYear();
        int month = entity.getGpsTimeLocal().getMonthValue();
        int dayOfMonth = entity.getGpsTimeLocal().getDayOfMonth();
        int year = entity.getGpsTimeLocal().getYear();
        return yearCurrent == year && monthCurrent == month && dayOfMonth == dayOfMonthCurrent;
    }

    public void getDataForReportCombinate(){
        RepUsuarioEntity user = repUsuarioRepository.findByUsuario("powerbimarcobre");
        List<RepVehiculoEntity> vehicleEntityList = repVehiculoRepository.findByRepUsuarioEntity(user);

        LocalDateTime dateFind = this.getDateFind();
        log.info("fecha a consultar: {}", dateFind);

        List<RepMarDataPositionListEntity> dataPosListDBList =
            repDataPositionListRepository.findByGpsTimeLocal(dateFind);
        log.info("cantidad de data traida desde  la bd: {}", dataPosListDBList.size());

        //filtrar solo los que tienen geocerca
        List<RepMarcobreDataPositionDTO> dataPostFilterListGeocerca = dataPosListDBList.stream()
            .map(positionDTO -> this.setGeocercaAndEventInDTO(vehicleEntityList, positionDTO))
            .collect(Collectors.toList());

        log.info("cantidad total a procesar despues del filtrado de geocerca: {}", dataPostFilterListGeocerca.size());

        //obtener agrupacion de items por placa y locacion
        Map<String, List<RepMarcobreDataPositionDTO>> groupFilterByPlaca =
            dataPostFilterListGeocerca.stream().collect(groupingBy(RepMarcobreDataPositionDTO::getUnitName));

        log.info("cantidad grupos de placas a procesar: {}", groupFilterByPlaca.size());

        LinkedHashMap<String, Map<String, List<RepMarcobreDataPositionDTO>>> groupMapByPlacaAndLocation =
            new LinkedHashMap<>();

        List<String> grupoPlaca = new ArrayList<>(groupFilterByPlaca.keySet());
        log.info("cantidad de placas a procesar {}", grupoPlaca.size());

        this.groupByPlaqueAndLocation(groupFilterByPlaca, groupMapByPlacaAndLocation, grupoPlaca);
        this.generateCombinateReport(groupMapByPlacaAndLocation, grupoPlaca);
    }

    private void generateCombinateReport(
        LinkedHashMap<String, Map<String, List<RepMarcobreDataPositionDTO>>> groupMapByPlacaAndLocation,
        List<String> grupoPlaca){
        log.info("###################################################################");
        log.info("2.- inicio generar reporte combinado ");
        log.info("###################################################################");
        for (String placa : grupoPlaca) {
            Map<String, List<RepMarcobreDataPositionDTO>> listLocation = groupMapByPlacaAndLocation.get(placa);
            log.info("lista grupo por placa: {}", listLocation);
            List<String> locations = new ArrayList<>(listLocation.keySet());
            List<RepMarcobreDataPositionDTO> listDataSubGrupo = new ArrayList<>();

            for (String location : locations) {
                List<RepMarcobreDataPositionDTO> listData = listLocation.get(location)
                    .stream()
                    .sorted(Comparator.comparing(RepMarcobreDataPositionDTO::getGpsTimeLocal))
                    .collect(Collectors.toList());
                log.info("-- lista grupo por placa locations: {}", listData);
                String eventoAnt = "";
                for (RepMarcobreDataPositionDTO item : listData) {
                    if (StringUtils.isEmpty(eventoAnt) && StringUtils.isEmpty(item.getEvent())) {
                        log.info("---no tiene evento: {}", item);
                        continue;
                    }
                    if (listDataSubGrupo.isEmpty()) {
                        log.info("---agregamos el primer item a la lista de grupo: {}", item);
                        listDataSubGrupo.add(item);
                    } else {
                        if (eventoAnt.equals(item.getEvent())) {
                            log.info("---si evento anterior es igual al siguiente, agregamos al mismo grupo: {}", item);
                            listDataSubGrupo.add(item);
                        } else {
                            log.info("---operar tiempo inicial y final para duracion del evento: {}", listDataSubGrupo);
                            //proceso la lista con los items
                            ExcelPositionModel excelPositionModel = getExcelPositionModel(listDataSubGrupo);
                            //insertar en tabla reporte_combinado
                            RepMarCombinadoEntity repMarCombinadoEntity = new RepMarCombinadoEntity();
                            repMarCombinadoEntity.setDuracion(LocalTime.parse(excelPositionModel.getDuracion()));
                            repMarCombinadoEntity.setConductor(excelPositionModel.getConductor());
                            repMarCombinadoEntity.setFechaFinal(excelPositionModel.getFechaFinal());
                            repMarCombinadoEntity.setFechaInicio(excelPositionModel.getFechaInicio());
                            repMarCombinadoEntity.setPlaca(excelPositionModel.getPlaca());
                            repMarCombinadoEntity.setLocalizacion(excelPositionModel.getLocalizacion());
                            repMarCombinadoEntity.setGrupoCliente(excelPositionModel.getGrupoCliente());
                            repMarCombinadoEntity.setEvento(excelPositionModel.getEvento());
                            repMarCombinadoEntity.setLimiteVelocidad(excelPositionModel.getLimiteVelocidad());
                            repMarCombinadoEntity.setRangoExceso(excelPositionModel.getRangoExceso());
                            repMarCombinadoEntity.setVelocidadMaxima(excelPositionModel.getVelocidadMaxima());
                            repMarCombinadoEntity.setVelocidadPromedioMs(Double.valueOf(excelPositionModel.getVelocidadPromedio()));
                            repMarCombinadoEntity.setCreatedAt(new Date());
                            repMarCombinadoEntity.setTag(excelPositionModel.getTag());
                            log.info("insertar reporte combinado: {}", repMarCombinadoEntity);
                            repCombinadoRepository.save(repMarCombinadoEntity);
                            listDataSubGrupo.clear(); // se limpia el grupo anterior
                            listDataSubGrupo.add(item); //agrego el item que tiene nuevo evento a la lista vacia
                        }
                    }
                    eventoAnt = item.getEvent();
                }
            }
        }
        log.info("###################################################################");
        log.info("2.- fin generar reporte combinado ");
        log.info("###################################################################");
    }

    private void groupByPlaqueAndLocation(
        Map<String, List<RepMarcobreDataPositionDTO>> groupFilterByPlaca,
        LinkedHashMap<String, Map<String, List<RepMarcobreDataPositionDTO>>> groupMapByPlacaAndLocation,
        List<String> grupoPlaca){
        log.info("###################################################################");
        log.info("1.0- inicio de agrupar por placa ");
        log.info("###################################################################");
        for (String placa : grupoPlaca) {
            List<RepMarcobreDataPositionDTO> grupoPorPlaca = groupFilterByPlaca.get(placa);
            log.info("cantidad de grupo por placa: {}", grupoPorPlaca.size());
            log.info("items grupo por placa: {}", grupoPorPlaca);
            List<RepMarcobreDataPositionDTO> listData = grupoPorPlaca
                .stream()
                .sorted(Comparator.comparing(RepMarcobreDataPositionDTO::getGpsTimeLocal))
                .collect(Collectors.toList());

            LinkedHashMap<String, List<RepMarcobreDataPositionDTO>> groupMapByPlaca2 = new LinkedHashMap<>();

            int posGroup = 0;
            List<RepMarcobreDataPositionDTO> listGroup = new ArrayList<>();
            String posPlacaAnt = "";
            String posLocationAnt = "";
            for (int i = 0; i < listData.size(); i++) {
                RepMarcobreDataPositionDTO dto = listData.get(i);
                boolean isGroupNewByLocation = i != 0 && !dto.getLocation().equalsIgnoreCase(posLocationAnt);
                if (isGroupNewByLocation) {
                    listGroup = new ArrayList<>();
                    posGroup++;
                }
                boolean isOtherPlaqueOfVehicle = !dto.getUnitName().equalsIgnoreCase(posPlacaAnt);
                if (isOtherPlaqueOfVehicle) {
                    posGroup = 0;
                }
                StringBuilder posSb = new StringBuilder();
                String pos =
                    (posGroup >= 10) ? String.valueOf(posGroup) : posSb.append("0").append(posGroup).toString();
                String key = new StringBuilder().append(dto.getLocation()).append("_").append(pos).toString();
                if (!dto.getLocation().equalsIgnoreCase(posLocationAnt) && groupMapByPlaca2.containsKey(key)) {
                    key = new StringBuilder().append(key).append("_").append(1).toString();
                }
                listGroup.add(dto);
                groupMapByPlaca2.put(key, listGroup);
                posPlacaAnt = dto.getUnitName();
                posLocationAnt = dto.getLocation();
            }
            groupMapByPlacaAndLocation.put(placa, groupMapByPlaca2);
        }
        log.info("agrupacion por placa y localizacion: {} ", groupMapByPlacaAndLocation);
        log.info("###################################################################");
        log.info("1.1- fin de agrupar por placa ");
        log.info("###################################################################");
    }

    private LocalDateTime getDateFind(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime current = LocalDateTime.now().minusDays(1);
        int dayOfMonth = current.getDayOfMonth();
        int monthValue = current.getMonthValue();
        String day = dayOfMonth > 10 ? String.valueOf(dayOfMonth) : "0" + dayOfMonth;
        String month = monthValue > 10 ? String.valueOf(monthValue) : "0" + monthValue;
        String fecha = current.getYear() + "-" + month + "-" + day + " 00:00:00";
        return LocalDateTime.parse(fecha, formatter);
    }

    private RepMarcobreDataPositionDTO setGeocercaAndEventInDTO(
        List<RepVehiculoEntity> vehicleEntityList,
        RepMarDataPositionListEntity positionDTO){
        //velocityLimit
        RepMarcobreDataPositionDTO dto = RepDataPositionListMapper.mapper.toDTO(positionDTO);

        this.setGroupAndTag(vehicleEntityList, positionDTO, dto);

        dto.setLocation(positionDTO.getPointOfInterestUnitName());

        //evento - LEVE - MODERADO - GRAVE
        double velocity = positionDTO.getSpeed();

        String[] geocercaValue = positionDTO.getPointOfInterestUnitName().toLowerCase().split("km/h");
        if (!StringUtils.isNumeric(geocercaValue[0].trim())) {
            log.info("NO TIENE UN GEOCERCA NUMERICO, NO SETEAMOS ESTADO");
            return dto;
        }
        int velocityLimit = Integer.parseInt(geocercaValue[0].trim());

        //camionetas
        boolean camioneta = !StringUtils.isEmpty(dto.getTag()) &&
            Arrays.asList(dto.getTag().toLowerCase().split(" ")).contains("camioneta");
        boolean limitNewByGeocerca =
            Boolean.TRUE.equals(camioneta) && (velocityLimit == 50 || velocityLimit == 60 || velocityLimit == 70);
        boolean isDifferentToGaritaPrincipal =
            !"60km/h VIA 1 GARITA PRINCIPAL MINA".equalsIgnoreCase(dto.getPointOfInterestUnitName());
        if (limitNewByGeocerca && isDifferentToGaritaPrincipal) {
            velocityLimit = 100;
        }

        dto.setVelocityLimit(velocityLimit);

        //evento
        int leve = velocityLimit + 5;
        int moderaded = leve + 5;
        dto.setEvent("");

        if (velocity > velocityLimit && velocity <= leve) {
            dto.setEvent("LEVE");
        } else if (velocity > leve && velocity <= moderaded) {
            dto.setEvent("MODERADO");
        } else if (velocity > moderaded) {
            dto.setEvent("GRAVE");
        }
        log.info(
            "positionDTO: {}, velocity: {}, velocityLimit: {}, evento: {}, tiempo: {}, positionDTO: {}, tag: {}",
            positionDTO.getId(),
            velocity,
            velocityLimit,
            dto.getEvent(),
            dto.getGpsTimeLocal(),
            positionDTO,
            dto.getTag());
        return dto;
    }

    private void setGroupAndTag(
        List<RepVehiculoEntity> vehicleEntityList,
        RepMarDataPositionListEntity positionDTO,
        RepMarcobreDataPositionDTO dto){
        //obtenemos el grupo - cliente
        RepVehiculoEntity repVehiculoEntity = vehicleEntityList.stream()
            .filter(vehicle -> positionDTO.getUnitName().equalsIgnoreCase(vehicle.getPlaca()))
            .findFirst().orElse(null);
        dto.setGroupClient(repVehiculoEntity == null ? null : repVehiculoEntity.getGrupo());
        dto.setTag(repVehiculoEntity == null ? null : repVehiculoEntity.getTag());
    }

    private ExcelPositionModel getExcelPositionModel(
        List<RepMarcobreDataPositionDTO> listDataSubGrupo){
        RepMarcobreDataPositionDTO itemInitial = listDataSubGrupo.get(0);
        RepMarcobreDataPositionDTO itemFinal = listDataSubGrupo.get(listDataSubGrupo.size() - 1);

        long time1 = Timestamp.valueOf(itemInitial.getGpsTimeLocal()).getTime();
        long time2 = Timestamp.valueOf(itemFinal.getGpsTimeLocal()).getTime();
        long timeResult = time2 - time1;

        //Se obtiene la duracion del tiempo de inicio y tiempo final del grupo de vehiculos
        String duration = getDuration(timeResult);

        OptionalDouble average =
            listDataSubGrupo.stream().mapToDouble(RepMarcobreDataPositionDTO::getSpeed).average();
        double velocidadPromedio = average.isPresent() ? average.getAsDouble() : 0;

        Optional<RepMarcobreDataPositionDTO> itemMax =
            listDataSubGrupo.stream().max(Comparator.comparing(RepMarcobreDataPositionDTO::getSpeed));
        double speedMax = itemMax.isPresent() ? itemMax.get().getSpeed() : 0;

        String conductor = itemFinal.getDriverFirstName() + " " + itemFinal.getDriverLastName();

        ExcelPositionModel excelPositionModel = new ExcelPositionModel();
        excelPositionModel.setPlaca(itemInitial.getUnitName());
        excelPositionModel.setFechaInicio(itemInitial.getGpsTimeLocal());
        excelPositionModel.setFechaFinal(itemFinal.getGpsTimeLocal());
        excelPositionModel.setDuracion(duration);
        excelPositionModel.setLocalizacion(itemFinal.getAddress());
        excelPositionModel.setVelocidadMaxima(Double.toString(speedMax));
        excelPositionModel.setVelocidadPromedio(String.valueOf(velocidadPromedio));
        excelPositionModel.setConductor(conductor);
        excelPositionModel.setLimiteVelocidad(String.valueOf(itemFinal.getVelocityLimit()));
        excelPositionModel.setEvento(itemFinal.getEvent());
        excelPositionModel.setGrupoCliente(itemFinal.getGroupClient());
        excelPositionModel.setTag(itemFinal.getTag());
        return excelPositionModel;
    }

    private String getDuration(long timeResult){
        long seconds = timeResult / 1000;
        long minutes = seconds / 60;
        long hours = minutes / 60;
        long days = hours / 24;
        long hour = hours % 24;
        long minute = minutes % 60;
        long second = seconds % 60;

        String formatHour =
            String.valueOf(hour).length() == 1 ? new StringBuilder("0").append(hour).toString() : String.valueOf(hour);
        String formatMinute = String.valueOf(minute).length() == 1 ? new StringBuilder("0").append(minute).toString()
            : String.valueOf(minute);
        String formatSecond = String.valueOf(second).length() == 1 ? new StringBuilder("0").append(second).toString()
            : String.valueOf(second);
        return new StringBuilder().append(formatHour)
            .append(":")
            .append(formatMinute)
            .append(":")
            .append(formatSecond)
            .toString();
    }

}
