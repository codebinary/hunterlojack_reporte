package com.hunterlojack.service;

import java.io.IOException;

public interface DataVehicleService {

    void getAllDataOfService() throws IOException;

}
