package com.hunterlojack;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class HlBiReportApplicationTests {
    
    @Test
    void contextLoads(){
        LocalDateTime now = LocalDateTime.now();
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss");
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("E,MMM dd yyyy hh:mm:ss");
    
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd MMM yyyy hh:mm:ss");
//        DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime();
        String format = now.format(formatter);
        System.out.println("hola: " + format);
    }
    
}
