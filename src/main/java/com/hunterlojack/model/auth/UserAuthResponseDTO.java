package com.hunterlojack.model.auth;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hunterlojack.model.StatusResponseDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserAuthResponseDTO {

    @JsonProperty("Status")
    private StatusResponseDTO statusResponseDTO;

    @JsonProperty("Result")
    private AuthResultModel authResultModel;

}
