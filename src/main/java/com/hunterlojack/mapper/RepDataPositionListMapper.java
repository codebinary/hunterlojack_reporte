package com.hunterlojack.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.hunterlojack.dto.RepMarcobreDataPositionDTO;
import com.hunterlojack.entity.RepMarDataPositionListEntity;
import com.hunterlojack.model.report.data.position.DataPositionResponseDTO;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    unmappedSourcePolicy = ReportingPolicy.IGNORE
)
public interface RepDataPositionListMapper {

    RepDataPositionListMapper mapper = Mappers.getMapper(RepDataPositionListMapper.class);

    @Mapping(target = "unitUid", source = "unitModel.uid")
    @Mapping(target = "unitName", source = "unitModel.name")
    @Mapping(target = "unitImei", source = "unitModel.imei")
    @Mapping(target = "driverUid", source = "driverModel.uid")
    @Mapping(target = "driverFirstName", source = "driverModel.firstName")
    @Mapping(target = "driverLastName", source = "driverModel.lastName")
    @Mapping(target = "driverCode", source = "driverModel.code")
    @Mapping(target = "pointOfInterestUid", source = "pointInterest.uid")
    @Mapping(target = "pointOfInterestUnitName", source = "pointInterest.name")
    RepMarDataPositionListEntity toEntity(DataPositionResponseDTO positionModel);

    RepMarcobreDataPositionDTO toDTO(RepMarDataPositionListEntity dto);

}
