package com.hunterlojack.model.report.data.position;

import java.math.BigInteger;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class PositionResultResponseDTO {

    @JsonProperty("Position")
    private List<DataPositionResponseDTO> dataPositionResponseDTOS;

    @JsonProperty("StartId")
    private BigInteger startId;

}
