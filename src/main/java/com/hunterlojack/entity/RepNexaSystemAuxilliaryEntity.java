package com.hunterlojack.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RPB_NEXA_SYSTEM_AUXILLIARY")
public class RepNexaSystemAuxilliaryEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String reportRunLogUid;
    private LocalDateTime runDate;

    private String vehiculo;
    private String conductor;
    private String inputOutput;
    private String horaInicio;
    private String velocidad;
    private String localizacionInicial;
    private String distanciaTotal;
    private LocalDateTime fecha;

    @JoinColumn(name = "usuario_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private RepUsuarioEntity repUsuarioEntity;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

}
