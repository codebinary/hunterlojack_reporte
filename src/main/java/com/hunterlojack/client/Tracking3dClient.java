package com.hunterlojack.client;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import com.hunterlojack.model.auth.AuthResultModel;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.auth.UserAuthResponseDTO;
import com.hunterlojack.model.report.data.LogDataResultResponseDTO;
import com.hunterlojack.model.report.data.ReportRunLogDataResponseDTO;
import com.hunterlojack.model.report.data.position.DataPositionListResponseDTO;
import com.hunterlojack.model.report.data.position.PositionResultResponseDTO;
import com.hunterlojack.model.report.list.LogListResultResponseDTO;
import com.hunterlojack.model.report.list.ReportRunLogListModel;

public interface Tracking3dClient {

    UserAuthResponseDTO authentication(
        String user,
        String pass) throws IOException;

    ReportRunLogListModel reportRunLogList(AuthResultModel auth);

    List<LogListResultResponseDTO> reportRunLogList(LoginRequestDTO loginRequestDTO);

    ReportRunLogDataResponseDTO reportRunLogData(
        AuthResultModel auth,
        String uid);

    Optional<LogDataResultResponseDTO> reportRunLogData(
        LoginRequestDTO loginRequestDTO,
        String uid);

    DataPositionListResponseDTO dataPositionList(AuthResultModel auth);

    DataPositionListResponseDTO dataPositionList(
        AuthResultModel auth,
        String startId);

    Optional<PositionResultResponseDTO> dataPositionList(
        LoginRequestDTO loginRequestDTO,
        String startId);

}
