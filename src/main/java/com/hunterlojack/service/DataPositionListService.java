package com.hunterlojack.service;

public interface DataPositionListService {

    void processDataPositionList();

    void getDataForReportCombinate();

}
