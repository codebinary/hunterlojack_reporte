package com.hunterlojack.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hunterlojack.entity.RepMarCombinadoEntity;

public interface RepCombinadoRepository extends JpaRepository<RepMarCombinadoEntity, Long> {

    @Query(
        "SELECT e FROM RepMarCombinadoEntity e " +
            "WHERE e.fechaInicio >= ?1 " +
            "AND e.grupoCliente <> '' " +
            "AND e.fechaFinal <= ?2 " +
            "AND e.placa not like '%BJH-842%' " +
            "AND e.placa not like '%AFT-814%' " +
            "AND e.grupoCliente not like '%MAR COBRE - TERCEROS%' " +
            "AND e.duracion >= '00:00:31'")
    List<RepMarCombinadoEntity> findByDateInitialAndFinal(
        LocalDateTime fechaInicio,
        LocalDateTime fechaFinal);

}
