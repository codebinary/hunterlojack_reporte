package com.hunterlojack.model.report.data;

import java.time.LocalDateTime;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class LogDataResultResponseDTO {

    @JsonProperty("ReportRunLogUid")
    private String reportRunLogUid;

    @JsonProperty("ScheduleName")
    private String scheduleName;

    @JsonProperty("ScheduleType")
    private String scheduleType;

    @JsonProperty("ReportType")
    private String reportType;

    @JsonProperty("RunDate")
    private LocalDateTime runDate;

    @JsonProperty("Results")
    private List<DataResultResponseDTO> dataResultResponsDTOS;

}
