package com.hunterlojack.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.hunterlojack.client.Tracking3dClient;
import com.hunterlojack.entity.RepNexaAuxilliaryEntity;
import com.hunterlojack.entity.RepUsuarioEntity;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.report.data.DataResultResponseDTO;
import com.hunterlojack.model.report.data.LogDataResultResponseDTO;
import com.hunterlojack.model.report.list.LogListResultResponseDTO;
import com.hunterlojack.repository.RepNexaAuxiliaryRepository;
import com.hunterlojack.repository.RepUsuarioRepository;
import com.hunterlojack.service.DataNexaAuxiliaryService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DataNexaAuxiliaryServiceImpl implements DataNexaAuxiliaryService {

    private final Tracking3dClient tracking3dClient;
    private final RepNexaAuxiliaryRepository repNexaAuxiliaryRepository;
    private final RepUsuarioRepository repUsuarioRepository;

    public DataNexaAuxiliaryServiceImpl(
        Tracking3dClient tracking3dClient,
        RepNexaAuxiliaryRepository repNexaAuxiliaryRepository,
        RepUsuarioRepository repUsuarioRepository){
        this.tracking3dClient = tracking3dClient;
        this.repNexaAuxiliaryRepository = repNexaAuxiliaryRepository;
        this.repUsuarioRepository = repUsuarioRepository;
    }

    @Override
    public void getAllDataOfService(){
        //obtener los usuarios de POWER BI
        RepUsuarioEntity user = repUsuarioRepository.findByUsuario("powerbinexa");

        //insertar nueva informacion
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setPass("power22!");
        loginRequestDTO.setUser("powerbinexa");

        //obtener informacion de reportLogList
        List<LogListResultResponseDTO> resultsTmp = tracking3dClient.reportRunLogList(loginRequestDTO);
        log.info("obtener informacion de reportLogList: {}", resultsTmp);

        LogListResultResponseDTO logListResultResponseDTO =
            resultsTmp.stream().filter(x -> {
                log.info("item para filtrar vehicle details: {}", x);
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime runDate = x.getRunDate();
                return "auxilliary".equalsIgnoreCase(x.getReportType())
                    && x.getScheduleName().toLowerCase().startsWith("powerbi")
                    && now.getMonthValue() == runDate.getMonthValue()
                    && now.getYear() == runDate.getYear()
                    && now.getDayOfMonth() == runDate.getDayOfMonth();
            }).findAny().orElse(null);

        if (logListResultResponseDTO == null) {
            log.info("no hay imformacion, logListResultReponseDTO is null");
            return;
        }

        LogDataResultResponseDTO logDataResultResponseDTO;
        do {
            logDataResultResponseDTO =
                tracking3dClient.reportRunLogData(loginRequestDTO, logListResultResponseDTO.getReportRunLogUid())
                    .orElse(null);
        } while (logDataResultResponseDTO == null);

        List<DataResultResponseDTO> dataResultResponsDTOS =
            logDataResultResponseDTO.getDataResultResponsDTOS() == null
                ? new ArrayList<>() : logDataResultResponseDTO.getDataResultResponsDTOS();

        for (DataResultResponseDTO dataResultResponseDTO : dataResultResponsDTOS) {
            List<String> columns = dataResultResponseDTO.getColumns();
            for (Map<String, String> row : dataResultResponseDTO.getRows()) {
                log.info("mapa para obtener informacion {}", row);
                RepNexaAuxilliaryEntity entity = new RepNexaAuxilliaryEntity();
                for (int i = 0; i < columns.size(); i++) {
                    String column = columns.get(i);
                    String value = row.get(column);
                    switch (i) {
                        case 0:
                            entity.setVehiculo(value);
                            break;
                        case 1:
                            entity.setConductor(value);
                            break;
                        case 2:
                            entity.setHoraInicio(value);
                            break;
                        case 3:
                            entity.setHoraFin(value);
                            break;
                        case 4:
                            entity.setInputOutput(value);
                            break;
                        case 5:
                            entity.setLocalizacionInicial(value);
                            break;
                        case 6:
                            entity.setTiempoTotal(value);
                            break;
                        case 7:
                            entity.setVelocidad(value);
                            break;
                        case 8:
                            entity.setDistanciaTotal(value);
                            break;
                        case 9:
                            entity.setLocalizacionFinal(value);
                            break;
                        default:
                            break;
                    }
                }
                entity.setReportRunLogUid(logDataResultResponseDTO.getReportRunLogUid());
                entity.setRunDate(logDataResultResponseDTO.getRunDate());
                entity.setFecha(
                    logDataResultResponseDTO.getRunDate() != null ? logDataResultResponseDTO.getRunDate().minusDays(1)
                        : LocalDateTime.now().minusDays(1));
                entity.setRepUsuarioEntity(user);
                repNexaAuxiliaryRepository.save(entity);
            }
        }
    }

}
