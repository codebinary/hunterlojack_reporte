package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaDetailedActivityEntity;

public interface RepNexaDetailedActivityRepository extends JpaRepository<RepNexaDetailedActivityEntity, Long> {

}
