package com.hunterlojack.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RPB_NEXA_DETAILED_ACTIVITY")
public class RepNexaDetailedActivityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String reportRunLogUid;
    private LocalDateTime runDate;

    private String dia;
    private String vehiculo;
    private String distanciaTotal;
    private String tiempoManejo;
    private String tiempoRalenti;
    private String tiempoMotorApagado;
    private String tiempoTotal;
    private String tiempoViaje;
    private String velocidadPromedio;
    private String horaInicio;
    private String horaFin;
    private String velocidadMaxima;
    private String localizacionFinal;
    private String localizacionInicial;
    private LocalDateTime fecha;

    @JoinColumn(name = "usuario_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private RepUsuarioEntity repUsuarioEntity;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

}
