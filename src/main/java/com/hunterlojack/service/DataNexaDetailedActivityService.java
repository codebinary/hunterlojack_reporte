package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaDetailedActivityService {

    void getAllDataOfService() throws IOException;

}
