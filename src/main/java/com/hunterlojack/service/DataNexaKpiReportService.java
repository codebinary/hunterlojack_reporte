package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaKpiReportService {

    void getAllDataOfService() throws IOException;

}
