package com.hunterlojack.mapper;

import java.text.DecimalFormat;
import java.time.LocalTime;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import com.hunterlojack.dto.ReporteCombinadoDTO;
import com.hunterlojack.entity.RepMarCombinadoEntity;
import com.hunterlojack.model.excel.ExcelPositionModel;

@Mapper(
    componentModel = "spring",
    unmappedTargetPolicy = ReportingPolicy.IGNORE,
    unmappedSourcePolicy = ReportingPolicy.IGNORE,
    imports = { LocalTime.class }
)
public interface RepCombinadoMapper {

    RepCombinadoMapper mapper = Mappers.getMapper(RepCombinadoMapper.class);

    @Mapping(target = "velocidadPromedio", source = "velocidadPromedioMs", qualifiedByName = "formatPromedio")
    @Mapping(target = "vehiculo", source = "placa")
    @Mapping(target = "duracionHms", source = "duracion", dateFormat = "HH:mm:ss")
    @Mapping(target = "rangoExceso", source = "evento")
    @Mapping(target = "tag", source = "tag")
    @Mapping(target = "fechaInicio", dateFormat = "dd-MM-yyyy HH:mm:ss")
    @Mapping(target = "fechaFinal", dateFormat = "dd-MM-yyyy HH:mm:ss")
    ReporteCombinadoDTO toDto(RepMarCombinadoEntity entity);

    @Mapping(target = "duracion", expression = "java(LocalTime.parse(entity.getDuracion()))")
    @Mapping(target = "velocidadPromedioMs",
        expression = "java(Double.valueOf(entity.getVelocidadPromedio()))")
    RepMarCombinadoEntity toEntity(ExcelPositionModel entity);

    @Named("formatPromedio")
    default double formatPromedio(Double value){
        DecimalFormat format = new DecimalFormat("#.00");
        return Double.parseDouble(format.format(value));
    }

}
