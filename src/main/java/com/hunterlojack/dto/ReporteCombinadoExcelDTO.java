package com.hunterlojack.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class ReporteCombinadoExcelDTO {

    @JsonProperty("url_file")
    private String urlFile;

}
