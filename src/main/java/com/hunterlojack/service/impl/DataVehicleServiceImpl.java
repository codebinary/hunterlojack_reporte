package com.hunterlojack.service.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.hunterlojack.client.Tracking3dClient;
import com.hunterlojack.entity.RepUsuarioEntity;
import com.hunterlojack.entity.RepVehiculoEntity;
import com.hunterlojack.model.auth.LoginRequestDTO;
import com.hunterlojack.model.report.data.DataResultResponseDTO;
import com.hunterlojack.model.report.data.LogDataResultResponseDTO;
import com.hunterlojack.model.report.list.LogListResultResponseDTO;
import com.hunterlojack.repository.RepUsuarioRepository;
import com.hunterlojack.repository.RepVehiculoRepository;
import com.hunterlojack.service.DataVehicleService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DataVehicleServiceImpl implements DataVehicleService {

    private final Tracking3dClient tracking3dClient;
    private final RepVehiculoRepository repVehiculoRepository;
    private final RepUsuarioRepository repUsuarioRepository;

    public DataVehicleServiceImpl(
        Tracking3dClient tracking3dClient,
        RepVehiculoRepository repVehiculoRepository,
        RepUsuarioRepository repUsuarioRepository){
        this.tracking3dClient = tracking3dClient;
        this.repVehiculoRepository = repVehiculoRepository;
        this.repUsuarioRepository = repUsuarioRepository;
    }

    @Override
    public void getAllDataOfService(){
        //obtener los usuarios de POWER BI
        RepUsuarioEntity user = repUsuarioRepository.findByUsuario("powerbimarcobre");

        //insertar nueva informacion
        LoginRequestDTO loginRequestDTO = new LoginRequestDTO();
        loginRequestDTO.setPass(user.getContrasena());
        loginRequestDTO.setUser(user.getUsuario());

        //obtener informacion de reportLogList
        List<LogListResultResponseDTO> resultsTmp = tracking3dClient.reportRunLogList(loginRequestDTO);
        log.info("obtener informacion de reportLogList: {}", resultsTmp);

        LogListResultResponseDTO logListResultResponseDTO =
            resultsTmp.stream().filter(x -> {
                log.info("item para filtrar vehicle details: {}", x);
                LocalDateTime now = LocalDateTime.now();
                LocalDateTime runDate = x.getRunDate();
                return "vehicle details".equalsIgnoreCase(x.getReportType())
                    && now.getMonthValue() == runDate.getMonthValue()
                    && now.getYear() == runDate.getYear()
                    && now.getDayOfMonth() == runDate.getDayOfMonth();
            }).findAny().orElse(null);

        if (logListResultResponseDTO == null) {
            log.info("no encuentran información de loglistresult");
            return;
        } else {
            repVehiculoRepository.deleteAllInBatch();
            log.info("eliminar todos los vehiculos");
        }

        LogDataResultResponseDTO logDataResultResponseDTO;
        do {
            logDataResultResponseDTO =
                tracking3dClient.reportRunLogData(loginRequestDTO, logListResultResponseDTO.getReportRunLogUid())
                    .orElse(null);
        } while (logDataResultResponseDTO == null);

        List<DataResultResponseDTO> dataResultResponsDTOS =
            logDataResultResponseDTO.getDataResultResponsDTOS() == null
                ? new ArrayList<>() : logDataResultResponseDTO.getDataResultResponsDTOS();

        for (DataResultResponseDTO dataResultResponseDTO : dataResultResponsDTOS) {
            List<String> columns = dataResultResponseDTO.getColumns();
            for (Map<String, String> row : dataResultResponseDTO.getRows()) {
                log.info("mapa para obtener informacion {}", row);
                RepVehiculoEntity repVehiculoEntity = new RepVehiculoEntity();
                for (int i = 0; i < columns.size(); i++) {
                    String column = columns.get(i);
                    String value = row.get(column);
                    switch (i) {
                        case 0:
                            repVehiculoEntity.setPlaca(value);
                            break;
                        case 1:
                            repVehiculoEntity.setGrupo(value);
                            break;
                        case 2:
                            repVehiculoEntity.setOdometro(value);
                            break;
                        case 3:
                            repVehiculoEntity.setInformationLast(value);
                            break;
                        case 4:
                            repVehiculoEntity.setLocation(value);
                            break;
                        case 5:
                            repVehiculoEntity.setTag(value);
                            break;
                        default:
                            break;
                    }
                }
                repVehiculoEntity.setRepUsuarioEntity(user);
                repVehiculoRepository.save(repVehiculoEntity);
            }
        }
    }

}
