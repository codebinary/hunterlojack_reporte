package com.hunterlojack.common;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.hunterlojack.exception.AppException;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class SFTPConnect {

    private static final String STRICT_HOST_KEY_CHECKING = "StrictHostKeyChecking";
    private static final String STRICT_HOST_KEY_CHECKING_VALUE_NO = "no";
    private static final String CHANNEL_SFTP = "sftp";
    private static final String PUBLICKEY = "publickey";
    private static final String PREFERRED_AUTHENTICATIONS = "PreferredAuthentications";

    @Value("${app.sftp.extranet.hunter.host}")
    private String sftpHost;

    @Value("${app.sftp.extranet.hunter.port}")
    private String sftpPort;

    @Value("${app.sftp.extranet.hunter.user}")
    private String sftpUser;

    @Value("${app.sftp.extranet.hunter.psw}")
    private String sftpPass;

    public void uploadFile(
        String fileNameLocal,
        String fileNameRemote){
        log.info("inicio subir ficheros");
        SFTPConnectSession session = null;
        try {
            session = connectSFTP();
            session.getChannelSFTP().put(fileNameLocal, fileNameRemote);
            disconnectSFTP(session);
            log.info("fin subir ficheros");
        } catch (JSchException e) {
            disconnectSFTP(session);
            throw new AppException("error al abrir conexion sftp fichero [" + fileNameLocal + "]", e);
        } catch (SftpException e) {
            disconnectSFTP(session);
            throw new AppException("error al subir fichero [" + fileNameLocal + "]", e);
        }
    }

    public void uploadFile(
        InputStream fileNameLocal,
        String fileNameRemote){
        log.info("inicio subir ficheros");
        SFTPConnectSession session = null;
        try {
            session = connectSFTP();
            session.getChannelSFTP().put(fileNameLocal, fileNameRemote);
            disconnectSFTP(session);
            log.info("fin subir ficheros");
        } catch (JSchException e) {
            disconnectSFTP(session);
            throw new AppException("error al abrir conexion sftp fichero [" + fileNameLocal + "]", e);
        } catch (SftpException e) {
            disconnectSFTP(session);
            throw new AppException("error al subir fichero [" + fileNameLocal + "]", e);
        }
    }

    public void downloadFile(
        String fileNameLocal,
        String fileNameRemote){
        InputStream contenidoFicheroRemoto = null;
        try {
            log.info(
                "descargando desde SFTP ruta origen Remoto = {} , ruta destino filesystem = {}",
                fileNameRemote,
                fileNameLocal);
            contenidoFicheroRemoto = connectSFTP().getChannelSFTP().get(fileNameRemote);
            log.info("inputStream obtenido");
            log.info("procede a crear fichero en filesystem");
            Files.copy(contenidoFicheroRemoto, Paths.get(fileNameLocal));
            if (contenidoFicheroRemoto != null) {
                contenidoFicheroRemoto.close();
            }
            log.info("fichero creado en filesystem");
        } catch (IOException ioe) {
            closeInputStream(contenidoFicheroRemoto);
            throw new AppException("IOException error al descargar fichero [" + fileNameLocal + "]", ioe);
        } catch (SftpException e) {
            closeInputStream(contenidoFicheroRemoto);
            throw new AppException("SftpException error al descargar fichero [" + fileNameLocal + "]", e);
        } catch (JSchException e) {
            closeInputStream(contenidoFicheroRemoto);
            throw new AppException("JSchException error al descargar fichero [" + fileNameLocal + "]", e);
        }
    }

    private void closeInputStream(
        InputStream contenidoFicheroRemoto){
        if (contenidoFicheroRemoto != null) {
            try {
                contenidoFicheroRemoto.close();
            } catch (IOException e) {
                log.error("error al cerrar inputStream", e);
            }
        }
    }

    public void removeFile(
        String fileNameRemote){
        try {
            connectSFTP().getChannelSFTP().rm(fileNameRemote);
        } catch (SftpException e) {
            throw new AppException("error al borrar fichero [" + fileNameRemote + "]", e);
        } catch (JSchException e) {
            throw new AppException("error al borrar fichero [" + fileNameRemote + "]", e);
        }
    }

    @SuppressWarnings("unchecked")
    public List<LsEntry> ls(
        final String pathRemote){
        try {

            List<LsEntry> listFolders = new ArrayList<>(connectSFTP().getChannelSFTP().ls(pathRemote));
            if (CollectionUtils.isEmpty(listFolders)) {
                return new ArrayList<>();
            }
            return listFolders;
        } catch (SftpException | JSchException e) {
            throw new AppException("error al consultar directorios y consultas ", e);
        }
    }

    public List<LsEntry> findDir(
        final String pathRemote,
        Predicate<LsEntry> test){
        List<LsEntry> listTemp = null;
        listTemp = ls(pathRemote);
        if (CollectionUtils.isEmpty(listTemp)) {
            return new ArrayList<>();
        }
        return listTemp.stream().filter(test).collect(Collectors.toList());
    }

    public void chmod(
        String fileNameRemote,
        String permission){
        SFTPConnectSession session = null;
        int permissions = Integer.parseInt(permission, 8);
        try {
            session = connectSFTP();
            session.getChannelSFTP().chmod(permissions, fileNameRemote);
            disconnectSFTP(session);
        } catch (JSchException e) {
            disconnectSFTP(session);
            throw new AppException("error al abrir conexion sftp fichero [" + fileNameRemote + "]", e);
        } catch (SftpException e) {
            disconnectSFTP(session);
            throw new AppException("error al asignar permisos al fichero [" + fileNameRemote + "]", e);
        }
    }

    private SFTPConnectSession connectSFTP() throws JSchException{

        SFTPConnectConfig config = adaptToSFTPConnectConfig();

        String host = config.getHost();
        String user = config.getUser();
        int port = config.getPort();
        String pwd = config.getPassword();
        String certificatePath = config.getCertificatePath();

        if (!StringUtils.isBlank(pwd)) {
            return generateSessionByUserPassword(host, user, port, pwd);
        }
        return generareSessionByCertificatePath(host, user, port, certificatePath);
    }

    public SFTPConnectConfig adaptToSFTPConnectConfig(){
        SFTPConnectConfig entity = new SFTPConnectConfig();
        entity.setHost(sftpHost);
        entity.setPort(Integer.parseInt(sftpPort));
        entity.setUser(sftpUser);
        entity.setPassword(sftpPass);
        entity.setCertificatePath("");
        return entity;
    }

    private SFTPConnectSession generateSessionByUserPassword(
        String host,
        String user,
        int port,
        String pwd) throws JSchException{
        SFTPConnectSession result = new SFTPConnectSession();
        Session session = getSessionByUserPassword(host, user, port, pwd);
        result.setSession(session);
        Channel channel = retrieveChannel(session);
        result.setChannel(channel);
        result.setChannelSFTP(retrieveChannelSFTP(channel));
        return result;
    }

    private Session getSessionByUserPassword(
        String host,
        String user,
        int port,
        String pwd) throws JSchException{
        JSch jsch = new JSch();
        Session session = jsch.getSession(user, host, port);
        session.setPassword(pwd);
        Properties configJsch = new Properties();
        configJsch.put(STRICT_HOST_KEY_CHECKING, STRICT_HOST_KEY_CHECKING_VALUE_NO);
        session.setConfig(configJsch);
        session.connect();
        return session;
    }

    private SFTPConnectSession generareSessionByCertificatePath(
        String host,
        String user,
        int port,
        String certificatePath) throws JSchException{
        SFTPConnectSession result = new SFTPConnectSession();
        Session session = getSessionByCertificatePath(host, user, port, certificatePath);
        result.setSession(session);
        Channel channel = retrieveChannel(session);
        result.setChannel(channel);
        result.setChannelSFTP(retrieveChannelSFTP(channel));
        return result;
    }

    private Session getSessionByCertificatePath(
        String host,
        String user,
        int port,
        String certificatePath) throws JSchException{
        JSch jsch = new JSch();
        jsch.addIdentity(certificatePath);
        Session session = jsch.getSession(user, host, port);
        Properties configJsch = new Properties();
        configJsch.put(PREFERRED_AUTHENTICATIONS, PUBLICKEY);
        configJsch.put(STRICT_HOST_KEY_CHECKING, STRICT_HOST_KEY_CHECKING_VALUE_NO);
        session.setConfig(configJsch);
        session.connect();
        return session;
    }

    private ChannelSftp retrieveChannelSFTP(
        Channel channel){
        return (ChannelSftp) channel;
    }

    private Channel retrieveChannel(
        Session session) throws JSchException{
        Channel channel = session.openChannel(CHANNEL_SFTP);
        channel.connect();
        return channel;
    }

    private void disconnectSFTP(
        SFTPConnectSession sftpConnectionSession){

        if (Objects.isNull(sftpConnectionSession)) {
            return;
        }
        exitChannelSftp(sftpConnectionSession.getChannelSFTP());
        disconnectChannel(sftpConnectionSession.getChannel());
        disconnectSession(sftpConnectionSession.getSession());
    }

    private void exitChannelSftp(
        ChannelSftp channelSFTP){
        if (channelSFTP != null) {
            channelSFTP.exit();
        }
    }

    private void disconnectChannel(
        Channel channel){
        if (channel != null) {
            channel.disconnect();
        }
    }

    private void disconnectSession(
        Session session){
        if (session != null) {
            session.disconnect();
        }
    }

}
