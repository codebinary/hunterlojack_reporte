package com.hunterlojack.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.hunterlojack.entity.RepMarDataPositionListEntity;

public interface RepDataPositionListRepository extends JpaRepository<RepMarDataPositionListEntity, Long> {

    @Query("SELECT e FROM RepMarDataPositionListEntity e " +
        "WHERE e.gpsTimeLocal >= ?1 order by e.gpsTimeLocal asc")
    List<RepMarDataPositionListEntity> findByGpsTimeLocal(
        LocalDateTime localDateTime);

}
