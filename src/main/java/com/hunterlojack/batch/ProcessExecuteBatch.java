package com.hunterlojack.batch;

import java.io.IOException;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hunterlojack.service.DataPositionListService;
import com.hunterlojack.service.DataVehicleService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ProcessExecuteBatch {

    private final DataPositionListService dataPositionListService;
    private final DataVehicleService dataVehicleService;

    public ProcessExecuteBatch(
        DataPositionListService dataPositionListService,
        DataVehicleService dataVehicleService){
        this.dataPositionListService = dataPositionListService;
        this.dataVehicleService = dataVehicleService;
    }

    @Scheduled(cron = "0 0 1 * * *")
    public void processDataPosition(){
        log.info("1.- inicio de obtener datos del servicio");
        dataPositionListService.processDataPositionList();
        log.info("1.- fin de obtener datos del servicio");
    }

    @Scheduled(cron = "0 0 2 * * *")
    public void processDataReportCombinate(){
        log.info("3.- inicio de generar reporte combinado");
        dataPositionListService.getDataForReportCombinate();
        log.info("3.- fin de generar reporte combinado");
    }

    @Scheduled(cron = "0 30 1 * * *")
    public void processDataVehicle() throws IOException{
        log.info("2.- inicio de proceso de obtener vehiculos");
        this.dataVehicleService.getAllDataOfService();
        log.info("2.- fin de proceso de obtener vehiculos");
    }

}
