package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaKpiReportEntity;

public interface RepNexaKpiReportRepository extends JpaRepository<RepNexaKpiReportEntity, Long> {

}
