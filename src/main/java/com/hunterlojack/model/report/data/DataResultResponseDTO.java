package com.hunterlojack.model.report.data;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataResultResponseDTO {

    @JsonProperty("Title")
    private String title;

    @JsonProperty("Columns")
    private List<String> columns;

    @JsonProperty("Rows")
    private List<Map<String, String>> rows;

}
