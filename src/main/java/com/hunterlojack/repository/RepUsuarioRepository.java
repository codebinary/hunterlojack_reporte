package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepUsuarioEntity;

public interface RepUsuarioRepository extends JpaRepository<RepUsuarioEntity, Long> {

    RepUsuarioEntity findByUsuario(String user);

}
