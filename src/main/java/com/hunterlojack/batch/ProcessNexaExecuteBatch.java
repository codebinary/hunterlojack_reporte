package com.hunterlojack.batch;

import java.io.IOException;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.hunterlojack.service.DataNexaAuxiliaryService;
import com.hunterlojack.service.DataNexaDetailedActivityService;
import com.hunterlojack.service.DataNexaDriverBehaviourService;
import com.hunterlojack.service.DataNexaKpiReportService;
import com.hunterlojack.service.DataNexaSystemAuxiliaryService;
import com.hunterlojack.service.DataNexaVehicleService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class ProcessNexaExecuteBatch {

    private final DataNexaAuxiliaryService dataNexaAuxiliaryService;
    private final DataNexaDetailedActivityService dataNexaDetailedActivityService;
    private final DataNexaDriverBehaviourService dataNexaDriverBehaviourService;
    private final DataNexaSystemAuxiliaryService dataNexaSystemAuxiliaryService;
    private final DataNexaKpiReportService dataNexaKpiReportService;
    private final DataNexaVehicleService dataNexaVehicleService;

    public ProcessNexaExecuteBatch(
        DataNexaAuxiliaryService dataNexaAuxiliaryService,
        DataNexaDetailedActivityService dataNexaDetailedActivityService,
        DataNexaDriverBehaviourService dataNexaDriverBehaviourService,
        DataNexaSystemAuxiliaryService dataNexaSystemAuxiliaryService,
        DataNexaKpiReportService dataNexaKpiReportService,
        DataNexaVehicleService dataNexaVehicleService){
        this.dataNexaAuxiliaryService = dataNexaAuxiliaryService;
        this.dataNexaDetailedActivityService = dataNexaDetailedActivityService;
        this.dataNexaDriverBehaviourService = dataNexaDriverBehaviourService;
        this.dataNexaSystemAuxiliaryService = dataNexaSystemAuxiliaryService;
        this.dataNexaKpiReportService = dataNexaKpiReportService;
        this.dataNexaVehicleService = dataNexaVehicleService;
    }
    @Scheduled(cron = "0 0 2 * * *")
    public void processDataNexaAuxiliaryService() throws IOException{
        log.info("1.- processDataNexaAuxiliaryService - inicio de proceso");
        dataNexaAuxiliaryService.getAllDataOfService();
        log.info("1.- processDataNexaAuxiliaryService - fin de proceso");
    }

    @Scheduled(cron = "0 5 2 * * *")
    public void processDataNexaDetailedActivityService() throws IOException{
        log.info("2.- processDataNexaAuxiliaryService - inicio de proceso");
        dataNexaDetailedActivityService.getAllDataOfService();
        log.info("2.- processDataNexaAuxiliaryService - fin de proceso");
    }

    @Scheduled(cron = "0 10 2 * * *")
    public void processDataNexaDriverBehaviourService() throws IOException{
        log.info("3.- processDataNexaDriverBehaviourService - inicio de proceso");
        this.dataNexaDriverBehaviourService.getAllDataOfService();
        log.info("3.- processDataNexaDriverBehaviourService - fin de proceso");
    }

    @Scheduled(cron = "0 15 2 * * *")
    public void processDataNexaSystemAuxiliaryService() throws IOException{
        log.info("4.- processDataNexaSystemAuxiliaryService - inicio de proceso");
        this.dataNexaSystemAuxiliaryService.getAllDataOfService();
        log.info("4.- processDataNexaSystemAuxiliaryService - fin de proceso");
    }

    @Scheduled(cron = "0 20 2 * * *")
    public void processDataNexaKpiReportService() throws IOException{
        log.info("5.- processDataNexaKpiReportService - inicio de proceso");
        this.dataNexaKpiReportService.getAllDataOfService();
        log.info("5.- processDataNexaKpiReportService - fin de proceso");
    }

    @Scheduled(cron = "0 25 2 * * *")
    public void processDataNexaVehicleService() throws IOException{
        log.info("6.- processDataNexaVehicleService - inicio de proceso");
        this.dataNexaVehicleService.getAllDataOfService();
        log.info("6.- processDataNexaVehicleService - fin de proceso");
    }

}
