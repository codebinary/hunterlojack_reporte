package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaDriverBehaviourEntity;

public interface RepNexaDriverBehaviourRepository extends JpaRepository<RepNexaDriverBehaviourEntity, Long> {

}
