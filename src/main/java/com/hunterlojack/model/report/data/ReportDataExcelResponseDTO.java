package com.hunterlojack.model.report.data;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReportDataExcelResponseDTO {

    private String reportUid;
    private String scheduleName;
    private List<DataResultResponseDTO> tableData;

}
