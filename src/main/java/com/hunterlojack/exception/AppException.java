package com.hunterlojack.exception;

public class AppException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    private ExceptionDetail exceptionDetail;

    public AppException(){
        super();
    }

    public AppException(
        String message,
        Throwable thrw,
        boolean enableSuppression,
        boolean writableStackTrace){
        super(message, thrw, enableSuppression, writableStackTrace);
    }

    public AppException(
        String message,
        Throwable thrw){
        super(message, thrw);
    }

    public AppException(
        ExceptionDetail exceptionDetail,
        String message,
        Throwable thrw){
        super(message, thrw);
        this.exceptionDetail = exceptionDetail;
    }

    public AppException(
        String code,
        String messageCode,
        String message,
        Throwable thrw){
        this(ExceptionDetailValue.create(code, messageCode), message, thrw);
    }

    public AppException(
        ExceptionDetail exceptionDetail,
        String message){
        super(message);
        this.exceptionDetail = exceptionDetail;
    }

    public AppException(
        String code,
        String messageCode,
        String message){
        this(ExceptionDetailValue.create(code, messageCode), message);
    }

    public AppException(
        ExceptionDetail exceptionDetail,
        Throwable thrw){
        super(thrw);
        this.exceptionDetail = exceptionDetail;
    }

    public AppException(
        String code,
        String messageCode,
        Throwable thrw){
        this(ExceptionDetailValue.create(code, messageCode), thrw);
    }

    public AppException(String message){
        super(message);
    }

    public AppException(ExceptionDetail exceptionDetail){
        this.exceptionDetail = exceptionDetail;
    }

    public ExceptionDetail getExceptionDetail(){
        return this.exceptionDetail;
    }

}
