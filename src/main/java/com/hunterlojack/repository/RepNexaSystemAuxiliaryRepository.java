package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaSystemAuxilliaryEntity;

public interface RepNexaSystemAuxiliaryRepository extends JpaRepository<RepNexaSystemAuxilliaryEntity, Long> {

}
