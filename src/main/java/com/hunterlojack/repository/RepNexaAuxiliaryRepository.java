package com.hunterlojack.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaAuxilliaryEntity;

public interface RepNexaAuxiliaryRepository extends JpaRepository<RepNexaAuxilliaryEntity, Long> {

}
