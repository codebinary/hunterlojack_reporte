package com.hunterlojack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepUsuarioEntity;
import com.hunterlojack.entity.RepVehiculoEntity;

public interface RepVehiculoRepository extends JpaRepository<RepVehiculoEntity, Long> {

    List<RepVehiculoEntity> findByRepUsuarioEntity(RepUsuarioEntity usuario);

}
