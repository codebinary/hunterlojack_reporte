package com.hunterlojack.model.auth;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AuthResultModel {

    @JsonProperty("UserIdGuid")
    private String userIdGuid;

    @JsonProperty("SessionId")
    private String sessionId;

}
