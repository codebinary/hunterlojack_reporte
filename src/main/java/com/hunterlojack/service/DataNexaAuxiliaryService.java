package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaAuxiliaryService {

    void getAllDataOfService() throws IOException;

}
