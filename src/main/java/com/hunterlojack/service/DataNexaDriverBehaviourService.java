package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaDriverBehaviourService {

    void getAllDataOfService() throws IOException;

}
