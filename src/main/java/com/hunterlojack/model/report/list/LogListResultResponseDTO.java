package com.hunterlojack.model.report.list;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class LogListResultResponseDTO {

    @JsonProperty("ReportRunLogUid")
    private String reportRunLogUid;

    @JsonProperty("ScheduleName")
    private String scheduleName;

    @JsonProperty("ScheduleType")
    private String scheduleType;

    @JsonProperty("ReportType")
    private String reportType;

    @JsonProperty("RunDate")
    private LocalDateTime runDate;

}
