package com.hunterlojack.dto;

import java.time.LocalTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class ReporteCombinadoDTO {

    private Long id;

    @JsonProperty("vehiculo")
    private String vehiculo;

    @JsonProperty("fecha_inicio")
    private String fechaInicio;

    @JsonProperty("fecha_final")
    private String fechaFinal;

    private String localizacion;

    @JsonProperty("duracion")
    private LocalTime duracionHms;

    @JsonProperty("distancia")
    private Double distanciaKm;

    @JsonProperty("velocidad_maxima")
    private Double velocidadMaxima;

    @JsonProperty("velocidad_promedio")
    private Double velocidadPromedio;

    @JsonProperty("conductor")
    private String conductor;

    @JsonProperty("limite_velocidad")
    private Double limiteVelocidad;

    @JsonProperty("rango_exceso")
    private String rangoExceso;

    @JsonProperty("grupo_cliente")
    private String grupoCliente;

    @JsonProperty("tag")
    private String tag;

    @JsonProperty("exceso")
    private String exceso;

}
