package com.hunterlojack.model.excel;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ExcelPositionModel {

    private String placa;
    private LocalDateTime fechaInicio;
    private LocalDateTime fechaFinal;
    private String localizacion;
    private String duracion;
    private String velocidadMaxima;
    private String velocidadPromedio;
    private String conductor;
    private String limiteVelocidad;
    private String rangoExceso;
    private String grupoCliente;
    private String evento;
    private String tag;

}
