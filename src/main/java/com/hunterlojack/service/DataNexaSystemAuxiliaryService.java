package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaSystemAuxiliaryService {

    void getAllDataOfService() throws IOException;

}
