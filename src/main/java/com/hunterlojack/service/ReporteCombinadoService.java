package com.hunterlojack.service;

import java.util.List;

import com.hunterlojack.dto.ReporteCombinadoDTO;
import com.hunterlojack.dto.ReporteCombinadoExcelDTO;

public interface ReporteCombinadoService {

    List<ReporteCombinadoDTO> listByFechaInitialAndFinal(
        String client,
        String fecIni,
        String fecFin);

    ReporteCombinadoExcelDTO dataExportByDateInitialAndFinal(
        String client,
        String fechaInicial,
        String fechaFinal);

}
