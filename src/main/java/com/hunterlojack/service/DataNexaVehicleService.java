package com.hunterlojack.service;

import java.io.IOException;

public interface DataNexaVehicleService {

    void getAllDataOfService() throws IOException;

}
