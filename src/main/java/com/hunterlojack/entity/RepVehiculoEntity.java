package com.hunterlojack.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RPB_REPORTE_VEHICULO")
public class RepVehiculoEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String placa;

    private String grupo;

    private String odometro;

    private String informationLast;

    private String location;

    private String tag;

    @JoinColumn(name = "usuario_id")
    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private RepUsuarioEntity repUsuarioEntity;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

}
