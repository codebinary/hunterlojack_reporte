package com.hunterlojack.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.time.LocalDateTime;
import java.util.Date;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Entity
@Table(name = "RPB_MAR_DATA_POSITION_LIST")
public class RepMarDataPositionListEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String unitUid;
    private String unitName;
    private String unitImei;

    private Double latitude;
    private Double longitude;
    private String address;
    private Double speed;
    private String speedMeasure;
    private Double heading;
    private String ignition;
    private Double odometer;
    private Double engineTime;
    private String engineStatus;
    private LocalDateTime serverTimeUTC;
    private LocalDateTime gpsTimeLocal;
    private LocalDateTime gpsTimeUtc;

    private String driverUid;

    @Column(name = "driver_firstname")
    private String driverFirstName;
    @Column(name = "driver_lastname")
    private String driverLastName;

    private String driverCode;

    @Column(name = "pointofinterest_uid")
    private String pointOfInterestUid;

    @Column(name = "pointofinterestunit_name")
    private String pointOfInterestUnitName;

    private String startId;

    @Column(name = "created_at")
    @CreationTimestamp
    private Date createdAt;

    @Override
    public String toString(){
        return "|" + unitUid +
            "|" + unitName +
            "|" + unitImei +
            "|" + latitude +
            "|" + longitude +
            "|" + address +
            "|" + speed +
            "|" + speedMeasure +
            "|" + heading +
            "|" + ignition +
            "|" + odometer +
            "|" + engineTime +
            "|" + engineStatus +
            "|" + serverTimeUTC +
            "|" + gpsTimeLocal +
            "|" + gpsTimeUtc +
            "|" + driverUid +
            "|" + driverFirstName +
            "|" + driverLastName +
            "|" + driverCode +
            "|" + pointOfInterestUid +
            "|" + pointOfInterestUnitName +
            "|" + startId +
            "\n";
    }

}
