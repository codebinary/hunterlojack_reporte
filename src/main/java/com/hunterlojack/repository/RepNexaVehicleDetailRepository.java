package com.hunterlojack.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.hunterlojack.entity.RepNexaVehicleDetailEntity;
import com.hunterlojack.entity.RepUsuarioEntity;

public interface RepNexaVehicleDetailRepository extends JpaRepository<RepNexaVehicleDetailEntity, Long> {

    List<RepNexaVehicleDetailEntity> findByRepUsuarioEntity(RepUsuarioEntity usuario);

}
