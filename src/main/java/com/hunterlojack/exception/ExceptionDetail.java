package com.hunterlojack.exception;

import java.io.Serializable;

public interface ExceptionDetail extends Serializable {

    String getCode();

    String getMessage();

    String toMessageComplete();

}
