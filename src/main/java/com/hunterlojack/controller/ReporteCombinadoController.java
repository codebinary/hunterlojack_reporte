package com.hunterlojack.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hunterlojack.dto.ReporteCombinadoDTO;
import com.hunterlojack.dto.ReporteCombinadoExcelDTO;
import com.hunterlojack.service.ReporteCombinadoService;

@RestController
@RequestMapping("/v1/combinate")
public class ReporteCombinadoController {

    private final ReporteCombinadoService reporteCombinadoService;

    public ReporteCombinadoController(ReporteCombinadoService reporteCombinadoService){
        this.reporteCombinadoService = reporteCombinadoService;
    }

    @GetMapping("/report")
    public ResponseEntity<List<ReporteCombinadoDTO>> findAll(
        @RequestParam("client") String client,
        @RequestParam("fec_ini") String fecIni,
        @RequestParam("fec_fin") String fecFin){
        return ResponseEntity.ok(reporteCombinadoService.listByFechaInitialAndFinal(fecIni, fecFin, client));
    }

    @GetMapping("/export")
    public ResponseEntity<ReporteCombinadoExcelDTO> export(
        @RequestParam("client") String client,
        @RequestParam("fec_ini") String fecIni,
        @RequestParam("fec_fin") String fecFin){
        ReporteCombinadoExcelDTO urlFile = reporteCombinadoService.dataExportByDateInitialAndFinal(fecIni, fecFin, client);
        return ResponseEntity.ok().body(urlFile);
    }

}
