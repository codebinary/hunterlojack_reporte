package com.hunterlojack.model.report.data.position;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@JsonIgnoreProperties(ignoreUnknown = true)
public class DataPositionResponseDTO {

    @JsonProperty("Unit")
    private UnitModel unitModel;

    @JsonProperty("Latitude")
    private Double latitude;

    @JsonProperty("Longitude")
    private Double longitude;

    @JsonProperty("Address")
    private String address;

    @JsonProperty("Speed")
    private Double speed;

    @JsonProperty("SpeedMeasure")
    private String speedMeasure;

    @JsonProperty("Heading")
    private Double heading;

    @JsonProperty("Ignition")
    private String ignition;

    @JsonProperty("Odometer")
    private Double odometer;

    @JsonProperty("EngineTime")
    private Double engineTime;

    @JsonProperty("EngineStatus")
    private String engineStatus;

    @JsonProperty("ServerTimeUTC")
    private LocalDateTime serverTimeUTC;

    @JsonProperty("GPSTimeLocal")
    private String gpsTimeLocal;

    @JsonProperty("GPSTimeUtc")
    private LocalDateTime gpsTimeUtc;

    @JsonProperty("Driver")
    private DriverModel driverModel;

    @JsonProperty("PointOfInterest")
    private PointInterest pointInterest;

    @Getter
    @Setter
    @ToString
    public static class DriverModel {

        @JsonProperty("Uid")
        private String uid;

        @JsonProperty("FirstName")
        private String firstName;

        @JsonProperty("LastName")
        private String lastName;

        @JsonProperty("Code")
        private String code;

    }

    @Getter
    @Setter
    @ToString
    public static class PointInterest {

        @JsonProperty("Uid")
        private String uid;

        @JsonProperty("Name")
        private String name;

    }

    @Getter
    @Setter
    @ToString
    public static class UnitModel {

        @JsonProperty("Uid")
        private String uid;

        @JsonProperty("Name")
        private String name;

        @JsonProperty("Imei")
        private String imei;

    }

}
