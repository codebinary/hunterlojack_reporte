package com.hunterlojack.common;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.Session;

public class SFTPConnectSession {

    private Session session;
    private Channel channel;
    private ChannelSftp channelSFTP;

    public Session getSession(){
        return session;
    }

    public void setSession(
        Session session){
        this.session = session;
    }

    public Channel getChannel(){
        return channel;
    }

    public void setChannel(
        Channel channel){
        this.channel = channel;
    }

    public ChannelSftp getChannelSFTP(){
        return channelSFTP;
    }

    public void setChannelSFTP(
        ChannelSftp channelSFTP){
        this.channelSFTP = channelSFTP;
    }

}
