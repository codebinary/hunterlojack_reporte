package com.hunterlojack.constant;

public class AppConstant {

    public static final String GUION = "-";
    public static final String SPACE = " ";
    public static final String PREFIX_POWERBI = "POWERBI" + SPACE + GUION + SPACE;
    public static final String PUNTO = ".";
    public static final String EXT_XLS = PUNTO + "xls";

}
