package com.hunterlojack;

import javax.annotation.PostConstruct;

import java.util.TimeZone;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.client.RestTemplate;

@EnableAsync
@EnableScheduling
@SpringBootApplication
public class HlBiReportApplication {

    public static void main(String[] args){
        SpringApplication.run(HlBiReportApplication.class, args);
    }

    @PostConstruct
    void init(){
        TimeZone.setDefault(TimeZone.getTimeZone("America/Lima"));
    }

    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
