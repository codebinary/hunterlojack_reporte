package com.hunterlojack.exception;

public final class ExceptionDetailValue implements ExceptionDetail {

    private static final long serialVersionUID = 1L;

    private ExceptionDetailValue(
        String code,
        String message){
        this.code = code;
        this.message = message;
    }

    public static ExceptionDetail create(
        String code,
        String message){
        return new ExceptionDetailValue(code, message);
    }

    private final String code;
    private final String message;

    @Override
    public String getCode(){
        return this.code;
    }

    @Override
    public String getMessage(){
        return message;
    }

    @Override
    public String toMessageComplete(){
        return this.code + "-" + this.message;
    }

}
