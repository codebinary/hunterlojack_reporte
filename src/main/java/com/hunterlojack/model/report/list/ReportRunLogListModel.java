package com.hunterlojack.model.report.list;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hunterlojack.model.StatusResponseDTO;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
@ToString
public class ReportRunLogListModel {

    @JsonProperty("Status")
    private StatusResponseDTO statusResponseDTO;

    @JsonProperty("Result")
    private List<LogListResultResponseDTO> results;

}
