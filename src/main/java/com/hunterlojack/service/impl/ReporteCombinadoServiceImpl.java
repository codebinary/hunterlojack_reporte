package com.hunterlojack.service.impl;

import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hunterlojack.common.SFTPConnect;
import com.hunterlojack.constant.AppConstant;
import com.hunterlojack.dto.ReporteCombinadoDTO;
import com.hunterlojack.dto.ReporteCombinadoExcelDTO;
import com.hunterlojack.entity.RepMarCombinadoEntity;
import com.hunterlojack.mapper.RepCombinadoMapper;
import com.hunterlojack.repository.RepCombinadoRepository;
import com.hunterlojack.service.ReporteCombinadoService;

import lombok.extern.slf4j.Slf4j;

import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

@Slf4j
@Service
public class ReporteCombinadoServiceImpl implements ReporteCombinadoService {

    @Value("${app.folder.name.excel}")
    private String pathFolderLocal;
    @Value("${app.sftp.extranet.hunter.path}")
    private String pathFolderRemoteHunter;
    private final RepCombinadoRepository repCombinadoRepository;
    private final SFTPConnect sftpConnect;

    public ReporteCombinadoServiceImpl(
        RepCombinadoRepository repCombinadoRepository,
        SFTPConnect sftpConnect){
        this.repCombinadoRepository = repCombinadoRepository;
        this.sftpConnect = sftpConnect;
    }

    @Override
    public List<ReporteCombinadoDTO> listByFechaInitialAndFinal(
        String fechaInicial,
        String fechaFinal,
        String client){
        LocalDateTime dateTime1 = LocalDateTime.parse(
            new StringBuilder(fechaInicial).append(" 00:00:00"),
            DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        LocalDateTime dateTime2 = LocalDateTime.parse(
            new StringBuilder(fechaFinal).append(" 23:59:59"),
            DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        List<RepMarCombinadoEntity> list =
            repCombinadoRepository.findByDateInitialAndFinal(dateTime1, dateTime2);

        if (StringUtils.isNotBlank(client)) {
            return list.stream().map(RepCombinadoMapper.mapper::toDto).collect(
                    collectingAndThen(
                        toCollection(() -> new TreeSet<>(comparing(ReporteCombinadoDTO::getFechaInicio))), ArrayList::new)
                ).stream()
                .sorted(comparing(ReporteCombinadoDTO::getFechaInicio))
                .collect(Collectors.toList())
                .stream().filter(x -> client.equalsIgnoreCase(x.getGrupoCliente())).collect(Collectors.toList());
        }

        return list.stream().map(RepCombinadoMapper.mapper::toDto).collect(
                collectingAndThen(
                    toCollection(() -> new TreeSet<>(comparing(ReporteCombinadoDTO::getFechaInicio))), ArrayList::new)
            ).stream()
            .sorted(comparing(ReporteCombinadoDTO::getFechaInicio))
            .collect(Collectors.toList());
    }

    @Override
    public ReporteCombinadoExcelDTO dataExportByDateInitialAndFinal(
        String fechaInicial,
        String fechaFinal,
        String client){
        log.info("###################################################");
        log.info("INICIO PROCESO DE GENERACION DE EXCEL");
        log.info("###################################################");
        LocalDateTime dateTime1 = LocalDateTime.parse(
            new StringBuilder(fechaInicial).append(" 00:00:00"),
            DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));
        LocalDateTime dateTime2 = LocalDateTime.parse(
            new StringBuilder(fechaFinal).append(" 23:59:59"),
            DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss"));

        List<RepMarCombinadoEntity> dataByExport =
            repCombinadoRepository.findByDateInitialAndFinal(dateTime1, dateTime2);
        List<ReporteCombinadoDTO> dtoListTmp =
            dataByExport.stream()
                .map(RepCombinadoMapper.mapper::toDto)
                .collect(
                    collectingAndThen(
                        toCollection(() -> new TreeSet<>(comparing(ReporteCombinadoDTO::getFechaInicio))),
                        ArrayList::new)
                )
                .stream()
                .sorted(comparing(ReporteCombinadoDTO::getFechaInicio))
                .collect(Collectors.toList());

        //filtramos los registros que no tienen grupo
        List<ReporteCombinadoDTO> dtoList = dtoListTmp.stream()
            .filter(item -> !StringUtils.isEmpty(item.getGrupoCliente()))
            .collect(Collectors.toList());

        if(StringUtils.isNotBlank(client)){
            dtoList = dtoListTmp.stream()
                .filter(item -> client.equalsIgnoreCase(item.getGrupoCliente()))
                .collect(Collectors.toList());
        }

        String[] columns = {
            "VEHICULO",
            "EMPRESA",
            "FECHA INICIO",
            "FECHA FINAL",
            "VELOCIDAD MAXIMA (km/h)",
            "VELOCIDAD PROMEDIO (km/h)",
            "DURACION (hh:mm:ss)",
            "LOCALIZACION",
            "CONDUCTOR",
            "ESTADO",
            "LIMITE PERMITIDA",
            "EXCESO"
        };

        Workbook workbook = new HSSFWorkbook();

        Sheet sheet = workbook.createSheet("reporte");
        Row row = sheet.createRow(0);

        //Creamos las columnas iniciales de la cabecera del excel
        for (int i = 0; i < columns.length; i++) {
            Cell cell = row.createCell(i);
            cell.setCellValue(columns[i]);
        }

        for (int i = 0; i < dtoList.size(); i++) {
            int position = i + 1;
            row = sheet.createRow(position);

            ReporteCombinadoDTO dto = dtoList.get(i);
            String location = dto.getLocalizacion();
            log.info("Direccion: {}", location);
            int indexKMH = location.toLowerCase().indexOf("km/h");
            int indexGeo = indexKMH - 3;
            String locationTmp = location.substring(indexGeo);
            String locationFinal;
            int indexCorchete = locationTmp.indexOf("[");
            if (indexCorchete == -1) {
                locationFinal = locationTmp;
            } else {
                locationFinal = locationTmp.substring(0, indexCorchete);
            }

            String localizacion =
                StringUtils.replaceEach(locationFinal, new String[] { "(", ")" }, new String[] { "", "" });
            String duracionTmp = dto.getDuracionHms().toString();
            String duracion = duracionTmp.length() == 5 ?
                new StringBuilder().append(duracionTmp).append(":00").toString() : duracionTmp;
            String velocidadPromedio = BigDecimal.valueOf(dto.getVelocidadPromedio())
                .setScale(2, RoundingMode.HALF_UP)
                .toString();

            Double velocidadMaxima = dto.getVelocidadMaxima();
            Double limiteVelocidad = dto.getLimiteVelocidad();

            double exceso = velocidadMaxima - limiteVelocidad;

            row.createCell(0).setCellValue(dto.getVehiculo());
            row.createCell(1).setCellValue(dto.getGrupoCliente());
            row.createCell(2).setCellValue(dto.getFechaInicio());
            row.createCell(3).setCellValue(dto.getFechaFinal());
            row.createCell(4).setCellValue(velocidadMaxima);
            row.createCell(5).setCellValue(velocidadPromedio);
            row.createCell(6).setCellValue(duracion);
            row.createCell(7).setCellValue(localizacion);
            row.createCell(8).setCellValue(dto.getConductor());
            row.createCell(9).setCellValue(dto.getRangoExceso());
            row.createCell(10).setCellValue(limiteVelocidad);
            row.createCell(11).setCellValue(exceso);
        }

        //Escribimos el archivo en local
        String fileNamePathLocalExcel = pathFolderLocal + "reporte_" + System.currentTimeMillis() + AppConstant.EXT_XLS;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(fileNamePathLocalExcel);
            workbook.write(fileOutputStream);
            fileOutputStream.close();
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //generar excel y enviarlo al servidor de la web
        String fileNameRemote = "reporte_" + System.currentTimeMillis() + AppConstant.EXT_XLS;
        String fileNamePathRemote = pathFolderRemoteHunter + fileNameRemote;
        sftpConnect.uploadFile(fileNamePathLocalExcel, fileNamePathRemote);
        ReporteCombinadoExcelDTO reporteCombinadoExcelDTO = new ReporteCombinadoExcelDTO();
        reporteCombinadoExcelDTO.setUrlFile(fileNameRemote);

        log.info("###################################################");
        log.info("FIN PROCESO DE GENERACION DE EXCEL");
        log.info("###################################################");
        return reporteCombinadoExcelDTO;
    }

}
