package com.hunterlojack.dto;

import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RepMarcobreDataPositionDTO {

    private Long id;
    private String unitUid;
    private String unitName;
    private String unitImei;
    private Double latitude;
    private Double longitude;
    private String address;
    private Double speed;
    private String speedMeasure;
    private Double heading;
    private String ignition;
    private Double odometer;
    private Double engineTime;
    private String engineStatus;
    private LocalDateTime serverTimeUTC;
    private LocalDateTime gpsTimeLocal;
    private LocalDateTime gpsTimeUtc;
    private String driverUid;
    private String driverFirstName;
    private String driverLastName;
    private String driverCode;
    private String pointOfInterestUid;
    private String pointOfInterestUnitName;
    private String location;
    private String event;
    private String groupClient;
    private String tag;
    private BigInteger startId;
    private Date createdAt;

    private Integer velocityLimit;

}
